const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLList
} = require('graphql');
const GraphQLDate = require('graphql-date');
const fs = require('fs');
const dotty = require('dotty');

const dataFile = "data/data.json";
const data = JSON.parse(fs.readFileSync(dataFile, 'utf8')).data;

/* Author object definition */
const Author = new GraphQLObjectType({
  name: "Author",
  fields: () => ({
    name: {
      type: GraphQLString
    },
    gender: {
      type: GraphQLString
    }
  })
});

/* Book object definition */
const Book = new GraphQLObjectType({
  name: "Book",
  fields: () => ({
    title: {
      type: GraphQLString
    },
    genre: {
      type: GraphQLString
    },
    published: {
      type: GraphQLDate,
      resolve: (root) => new Date(root.published)
    },
    author: {
      type: Author
    }
  })
});

/* Define a query that allows consumers to get a list of books */
const Query = new GraphQLObjectType({
  name: "Query",
  fields: () => ({
    books: {
      type: new GraphQLList(Book),
      args: {
        genre: {
          name: 'Book genre',
          type: GraphQLString
        },
        authorGender: {
          name: 'Author gender',
          type: GraphQLString
        },
        publishedDate: {
          name: "Publish date",
          type: GraphQLString
        },
        sort: {
          name: 'Sort field',
          type: GraphQLString
        },
        sortDirection: {
          name: 'Sort direction',
          type: GraphQLString
        },
        count: {
          name: 'Number of books to return',
          type: GraphQLInt
        },
        offset: {
          name: 'Offset from start of dataset',
          type: GraphQLInt
        }
      },
      resolve(_, { genre, authorGender: gender, publishedDate, sort, sortDirection,
          count = 100, offset = 0 }) {
        var processed = data.filter(book => {
          // Perform an exact match on genre and author.gender but only
          // check for the same day when looking at the publishDate
          return (!genre || book.genre.toLowerCase() === genre.toLowerCase()) &&
            (!gender || book.author.gender.toLowerCase() === gender.toLowerCase()) &&
            (!publishedDate || new Date(book.published).setHours(0,0,0,0) ===
              new Date(publishedDate).setHours(0,0,0,0));
        });
        if (sort) {
          let fn = (a, b) => {
            if (a[sort] < b[sort])
              return (sortDirection === "asc") ? -1 : 1;
            if (a[sort] > b[sort])
              return (sortDirection === "asc") ? 1 : -1;
            return 0;
          };

          // If the sort arg contains a "." we have to use dotty function to lookup
          // the attribute which is v. slow!
          if (~sort.indexOf('.')) {
            fn = (a, b) => {
              if (dotty.get(a, sort) < dotty.get(b, sort))
                return (sortDirection === "asc") ? -1 : 1;
              if (dotty.get(a, sort) > dotty.get(b, sort))
                return (sortDirection === "asc") ? 1 : -1;
              return 0;
            }
          }
          processed = processed.sort(fn);
        }
        return processed.slice(offset, offset + count);
      }
    }
  })
});

module.exports = new GraphQLSchema({
  query: Query
});