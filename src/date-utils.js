const dateUtils = {

  /*
   * Returns an array of dates that correspond to the last friday of each month
   * between January of startYear and December of endYear
   */
  getLastFridaysList: function(startYear, endYear) {
    var curYear = startYear,
        curMonth,
        curDay,
        curDate,
        results = [];

    while (curYear <= endYear) {
      for (curMonth = 1; curMonth <= 12; curMonth++) {
        // This will be executed for every month of every year

        curDay = 0; // Start at zero, days are 1 based so 0 is the last day of the month

        // if we get to -6 we've gone a whole week so should have found a Friday
        while(curDay >= -6) {
          // Generate all dates as UTC to avoid timezone issues
          curDate = new Date(Date.UTC(curYear, curMonth, curDay));

          // Friday is 5 (days are zero-based on Sundays)
          if (curDate.getDay() === 5) {
            results.push(curDate);
            break;
          }
          curDay--;
        }
      }
      curYear++;
    }
    return results;
  },
};

module.exports = dateUtils;