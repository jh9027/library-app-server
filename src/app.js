const express = require('express');
const graphqlHTTP = require('express-graphql');
const cors = require('cors');
const schema = require('./schema');

const port = 3000;
const app = express();

app.use(cors());

app.use('/graphql', graphqlHTTP((req, res) => {
  return {
    schema: schema,
    graphiql: true
  };
}));

app.listen(port, function () {
  console.log(`Library server app listening on port ${port}...`);
});