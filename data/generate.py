import argparse
import datetime
import random
import json
from faker import Faker

GENRES = [
	'Horror',
	'Science fiction',
	'Satire',
	'Drama',
	'Action and Adventure',
	'Romance',
	'Mystery',
	'Finance'
]

def main():
	"""Generate a list of n 'dummy' book objects and output them in JSON format"""

	# Define the usage/arguments
	parser = argparse.ArgumentParser(description='Generate a JSON file of n dummy books.')
	parser.add_argument('number', metavar='n', type=int, help='the number of books to generate')
	args = parser.parse_args()

	fake = Faker()
	books = []

	# Generate the specified number of fake books, this is inefficient for large numbers but it
	# works for a one-off quick script.
	for i in range(args.number):
		title = fake.sentence(nb_words=6, variable_nb_words=True)
		genre = random.choice(GENRES)
		published = fake.date_time_this_century()
		gender = random.choice(["Male", "Female"])
		name = getattr(fake, 'name_%s' % gender.lower())()

		book = {
			"title": title,
			"genre": genre,
			"published": published,
			"author": {
				"name": name,
				"gender": gender
			}
		}
		books.append(book)

	with open("data.json", "w") as f:
		json.dump({"data": books}, f, default=json_serial)

def json_serial(obj):
  """JSON serializer for objects not serializable by default json code"""

  if isinstance(obj, datetime.datetime):
    serial = obj.isoformat()
    return serial
  raise TypeError ("Type not serializable")

if __name__ == "__main__":
  main()