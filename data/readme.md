# Data Generator Documentation

This folder contains a basic fake data generation script that produces an array of book and author data in the following format:

```
{
    "genre": "Mystery", 
    "published": "2015-12-23T11:37:33", 
    "author": {
        "gender": "Male", 
        "name": "Geoffrey Everett"
    }, 
    "title": "Suscipit ipsum temporibus porro quo non."
}
```

## Usage

1. Setup a Python virtual env e.g. `virtualenv venv` (This script was tested against 2.7 but should work with later versions)

2. Activate the vritual environment: `source venv/bin/activate`

3. Install the script requirements using pip: `pip install -r requirements.txt`

4. Run the script: `python generate.py <n>` where <n> is the number of books you wish to generate.

5. The generated data will be saved in `data.json`

## Performance

The current implementation is not optimised in any way. All 'n' data objects are generated in memory before being written to the file - when 'n' is very large this could be problematic! A couple of ideas for future optimisations:

- Use a generator so that objects are produced 'on the fly' and not stored in memory
- Generate objects in memory but chunk the writing so that only say 1,000 are in memory at any one time. Not 100% sure how JSON can be written in chunks using the standard `json.dump` function.