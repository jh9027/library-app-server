# Library App Server

This is a basic graphQL server based on [Express](http://expressjs.com/) that serves data for the [library-app client](https://bitbucket.org/jh9027/library-app).

## Prerequisites

The following is a rough list of prerequisites. You may have success with alternate versions but this is what has been tested:

- Node v4 or higher
- npm 3.10 or higher

## Usage

1. Run `npm install` to install dependencies
2. Data will be served from 'data/data.json'. The repo includes a 'data.json' file of one million books but a new one can be generated as per the instructions in 'data/readme.md'
3. The server can be started by running `node src/app.js`
4. The interactive GraphIQL interface can be viewed at '[http://localhost:3000/graphql](http://localhost:3000/graphql)'

## Query Arguments

The following arguments are avaiable on the `books` endpoint:

- genre: A string to match the `genre` attribute against. An exact match is performed.
- authorGender: A string to match the `author.gender` attribute against. An exact match is performed.
- publishedDate: A string in ISO 8601 format. A match will be performed against the data part only; if a time is specified it will be ignored.
- sort: A string containing the name of the attribute to sort. The default sort order is descending.
- sortDirection: A string of either "asc" or "desc" specifying the order in which to sort.
- count: An integer specifying the number of book objects to return. Defaults to 100. This argument can be used for paging/lazy loading.
- offset: An integer specifying the offset from which to start count. Defaults to 0. This argument can be used for paging/lazy loading.

## Example Queries

These queries can be run via the GraphIQL interface available at [http://localhost:3000/graphql](http://localhost:3000/graphql).

#### Return all details for the first 100 books

```
{
  books {
    title
    genre
    published
    author {
      name
      gender
    }
  }
}
```

#### Return the title for the next 100 books

```
{
  books(offset: 100) {
    title
  }
}
```

#### Return the title and  published date for the 1000 most recent books

```
{
  books(count: 1000, sort: "published", sortDirection: "desc") {
    title
    published
  }
}
```

#### Return the above query but only books in the 'Horror' genre

```
{
  books(count: 1000, sort: "published", sortDirection: "desc", genre: "Horror") {
    title
    published
    genre
  }
}
```

## Assumptions & Limitations

- GraphIQL is hardcoded to always be available.
- The code is based on the limited examples on the web. The code is a bit 'piecemeal' and is in no way production ready! I wanted to do a bit of experimentation with graphql and the code should be viewed as such :)
- The 'api' exposed from the server is entirely tailored to the task specified. It could (and should) be made more generic but due to time limitations it isn't.
- data.json doesn't contain any timezone info because python doesn't include it. This is fine for demo purposes but would need consideration before being used in a production system.
- The implementation of the `resolve` function is pretty resource intensive and a bit messy in terms of the filtering. This is mainly due to using a raw json data source - things would be better if using a proper datasource.